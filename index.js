const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 3001;

// [SECTION] Mongodb connection
// Syntax -> mongoose.connect("SRV LINK", {useNewUrlParser: true, useUnifiedTopology: true})

mongoose.connect("mongodb+srv://admin:admin123@zuitt.qsixkvp.mongodb.net/B217_to-do?retryWrites=true&w=majority", {
	useNewUrlParser : true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database"));

// [SECTION] Mongoose Schemas
// It determines th estructure of our documents to be stored in the database
// It acts as our data blueprint and guide

const taskSchema = new mongoose.Schema({
	name : {
		type: String,
		required: [true, "Task name is required"]
	},
	status: {
		type: String,
		default: "pending"
	}
});

// ACTIVITY ----------------

const userSchema = new mongoose.Schema({
	username : {
		type: String,
		required: [true, "User name is required"]
	},
	password: {
		type: String,
		required: [true, "password is required"]
	}
});

// ACTIVITY -------------------

// [SECTION] Models 
const Task = mongoose.model("Task", taskSchema);


// [SECTION] Creation of to do list routes

app.use(express.json());

// allows your app to read data from forms
app.use(express.urlencoded({extended:true}));

app.post("/tasks", (req, res) => {
	Task.findOne({name: req.body.name}, (err, result) =>{
		if(result != null && result.name == req.body.name){
			return res.send("Duplicate found");
		}else{
			let newTask = new Task({
				name: req.body.name
			});

			newTask.save((saveErr, saveTask) => {
				if(saveErr){
					return console.error(saveErr);
				}else{
					return res.status(201).send("New task created")
				}
			})
		}
	})
})

// ACTIVITY --------

const User = mongoose.model("User", userSchema);

app.post("/signup", (req, res) => {
	User.findOne({username: req.body.username}, (err, result) =>{
		if(result != null && result.username == req.body.username){
			return res.send("Duplicate found");
		}else{
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			});

			newUser.save((saveErr, saveUser) => {
				if(saveErr){
					return console.error(saveErr);
				}else{
					return res.status(201).send("New user registered")
				}
			})
		}
	})
})





// ACTIVITY ---------

// Get method
app.get("/tasks", (req, res) => {
	Task.find({}, (err, result) => {
		if(err){
			return console.log(err);
		}else{
			return res.status(200).json({
				data : result
			})
		}
	})
})

















app.listen(port, () => console.log(`Server running at port ${port}`));

